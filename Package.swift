// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ScalaAnalytics",
    platforms: [
        .iOS(.v9)
    ], products: [
        .library(
            name: "ScalaAnalytics",
            targets: ["ScalaAnalytics"]),
    ], dependencies: [], targets: [
        .binaryTarget(
            name: "ScalaAnalytics",
            path: "./Sources/Frameworks/ScalaAnalytics.xcframework"),
        .testTarget(
            name: "ScalaAnalyticsTests",
            dependencies: ["ScalaAnalytics"]),
    ], swiftLanguageVersions: [
        .v5
    ]
)
