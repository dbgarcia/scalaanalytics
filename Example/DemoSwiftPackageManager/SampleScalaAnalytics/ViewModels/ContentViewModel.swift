//
//  ContentViewModel.swift
//  SampleScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

import Foundation
import ScalaAnalytics

class ContentViewModel: ObservableObject {
    
    // MARK: Variables
    
    private let scalaAnalyticsClient = ScalaAnalyticsClient.shared
    private let apiHost = "https://localhost:3000"
    private let apiKey = "AX011ASAS"
    
    @Published private(set) var metadatas: [Metadata] = Metadata.allCases.sorted(by: <)
    
    // MARK: Setup Client
    
    func initScalaAnalyticsClient() {
        scalaAnalyticsClient.configure(apiHost: apiHost, apiKey: apiKey)
    }
    
    // MARK: Chose Flow Metadata
    
    func sendTrack(_ metadata: Metadata) {
        switch metadata {
        case .wishlist:
            self.sendWishlist()
        case .campaign:
            self.sendCampaign()
        case .event:
            self.sendEvent()
        case .order:
            self.sendOrder()
        case .page:
            self.sendPage()
        case .pdp:
            self.sendPDP()
        case .profile:
            self.sendProfile()
        case .search:
            self.sendSearch()
        case .shipping:
            self.sendShipping()
        case .utm:
            self.sendUTM()
        }
    }
}

// MARK: Send metadata to server
extension ContentViewModel {
    
    private func sendPage() {
        let metadata = ScalaAnalyticsMetadata()
        let page = ScalaAnalyticsPage()
        page.pagepath = "https://…./bermuda-t-926771.html?cor=2V"
        page.pagetype = .navegacao
        page.hostname = "seusite.com.br"
        page.pagetitle = "Esportes"
        page.previouspage = .landingPage
        
        metadata.page = page
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendSearch() {
        let metadata = ScalaAnalyticsMetadata()
        let search = ScalaAnalyticsSearch()
        
        search.key = "Tenis nike"
        search.category = "Calçados"
        search.group = "Calçados"
        search.subcategory = "Corrida / caminhada"
        search.results = "10"
        
        metadata.search = search
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendPDP() {
        let metadata = ScalaAnalyticsMetadata()
        let page = ScalaAnalyticsPage()
        page.pagepath = "https://…./bermuda-t-926771.html?cor=2V"
        page.pagetype = .detalheProduto
        metadata.page = page
        
        let pdp = ScalaAnalyticsPDP()
        pdp.sku = "92677103"
        pdp.name = "Bermuda Fatal Tecnológico 9921 - Masculina"
        pdp.category = "Casual"
        pdp.price = "69,99"
        pdp.stockt = "Normal"
        pdp.stocko = "CD"
        pdp.available = "True"
        
        metadata.pdp = pdp
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendOrder() {
        let metadata = ScalaAnalyticsMetadata()
        let order = ScalaAnalyticsOrder()
        
        let cartitem1 = ScalaAnalyticsCartItem()
        cartitem1.sku = "849250330389"
        cartitem1.description = "Patins Oxer Freestyle - In Line - Freestyle"
        cartitem1.category = "Skating"
        cartitem1.price = "329,99"
        cartitem1.quantity = "1"
        cartitem1.stockt = "Normal"
        cartitem1.stocko = "CD"
        cartitem1.fretev = "10,00"
        cartitem1.fretep = "9 dias úteis"
        
        let cartitem2 = ScalaAnalyticsCartItem()
        cartitem2.sku = "849250930300"
        cartitem2.description = "PTESTE"
        cartitem2.category = "Skating"
        cartitem2.price = "329,99"
        cartitem2.quantity = "1"
        cartitem2.stocko = "CD"
        cartitem2.fretev = "10,00"
        cartitem2.fretep = "9 dias úteis"
        
        
        order.transactionId = "987654321"
        order.revenue = "1000,00"
        order.shipping = "100,00"
        order.tax = "200,00"
        order.discount = "500,00"
        order.dateLast = "2020-01-02"
        order.postalcode = "22221-111"
        
        var listCartItems = [ScalaAnalyticsCartItem]()
        listCartItems.append(cartitem1)
        listCartItems.append(cartitem2)
        
        metadata.order = order
        metadata.cart = listCartItems
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendProfile() {
        let metadata = ScalaAnalyticsMetadata()
        let profile = ScalaAnalyticsProfile()
        
        profile.id = "145902417"
        profile.age = "39"
        profile.state = "PE"
        profile.gender = "Feminino"
        profile.postalcode = "22221-111"
        profile.email = "email@gmail.com"
        profile.language = "PT-BR"
        profile.name = "Luciana da Silva"
        
        metadata.profile = profile
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendCampaign() {
        let metadata = ScalaAnalyticsMetadata()
        let campaign = ScalaAnalyticsCampaign()
        
        campaign.name = "Nome Campanha"
        metadata.campaign = campaign
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendWishlist() {
        let metadata = ScalaAnalyticsMetadata()
        var wishlistItems = [ScalaAnalyticsWishlist]()
        wishlistItems.append(ScalaAnalyticsWishlist("849250930300", "10,00"))
        wishlistItems.append(ScalaAnalyticsWishlist("849250930301", "20,00"))
        wishlistItems.append(ScalaAnalyticsWishlist("849250930302", "30,00"))
        
        metadata.wishlist = wishlistItems
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendUTM() {
        let metadata = ScalaAnalyticsMetadata()
        let utm = ScalaAnalyticsUTM()
        
        utm.medium = "3451609"
        utm.referralPath = "google"
        utm.campaign = "Parcerias Rakuten 1234"
        utm.source = "parcerias_rakuten"
        utm.keyword = "Rakuten123"
        
        metadata.utm = utm
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendEvent() {
        let metadata = ScalaAnalyticsMetadata()
        let event = ScalaAnalyticsEvent()
        
        event.category = "Banner - Home"
        event.action = "Click"
        event.name = "Escolha seu desconto"
        event.value = "0"
        
        metadata.event = event
        
        scalaAnalyticsClient.track(metadata)
    }
    
    private func sendShipping() {
        let metadata = ScalaAnalyticsMetadata()
        var shipping = [ScalaAnalyticsShippingItem]()
        
        let item1 = ScalaAnalyticsShippingItem()
        let item2 = ScalaAnalyticsShippingItem()
        
        let tipoe1 = ScalaAnalyticsShippingItem.Tipoe(nome: "Entrega Normal", prazo: "", frete: "10,00")
        let tipoe2 = ScalaAnalyticsShippingItem.Tipoe(nome: "Receba Amanhã", prazo: "")
        
        var listtipoe = [ScalaAnalyticsShippingItem.Tipoe]()
        listtipoe.append(tipoe1)
        listtipoe.append(tipoe2)
        
        item1.id = "1"
        item1.sku = "M0075S030001"
        item1.quantity = "1"
        item1.tipoe = listtipoe
        
        item2.id = "1"
        item2.sku = "M00762070005"
        item2.quantity = "2"
        item2.tipoe = listtipoe
        
        shipping.append(item1)
        shipping.append(item2)
        
        metadata.shipping = shipping
        
        scalaAnalyticsClient.track(metadata)
    }
}
