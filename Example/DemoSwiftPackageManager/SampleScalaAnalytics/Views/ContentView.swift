//
//  ContentView.swift
//  SampleScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

import SwiftUI

struct ContentView: View {

    private var viewModel = ContentViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.metadatas) { metadata in
                Text(metadata.name())
                    .padding()
                    .onTapGesture {
                        viewModel.sendTrack(metadata)
                    }
            }
            
            .onAppear {
                viewModel.initScalaAnalyticsClient()
            }
            
            .navigationBarTitle("Sample Scala Analytics", displayMode: .inline)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
