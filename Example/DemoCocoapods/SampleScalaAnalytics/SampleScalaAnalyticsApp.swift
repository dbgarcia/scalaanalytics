//
//  SampleScalaAnalyticsApp.swift
//  SampleScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

import SwiftUI

@main
struct SampleScalaAnalyticsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
