//
//  MetadataConfig.swift
//  SampleScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

import Foundation

protocol MetadataConfig {
    func name() -> String
}
