//
//  Metadata.swift
//  SampleScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

import Foundation

enum Metadata: String, CaseIterable, Comparable, Identifiable, MetadataConfig {
    
    case campaign
    case event
    case page
    case pdp
    case profile
    case order
    case search
    case shipping
    case utm
    case wishlist
    
    var id: String {
        return UUID().uuidString
    }
    
    func name() -> String {
        return self.rawValue.capitalized
    }
    
    static func < (lhs: Metadata, rhs: Metadata) -> Bool {
        return lhs.name() < rhs.name()
    }
}
