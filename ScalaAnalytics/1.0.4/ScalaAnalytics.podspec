
Pod::Spec.new do |s|

    s.name         = "ScalaAnalytics"
    s.version      = "1.0.4"
    s.summary      = "ScalaAnalytics"
    s.homepage     = "http://www.scalasystems.com.br/"
    s.author       = 'ScalaSystems'
    s.source       = { :git => 'https://gitlab.com/dbgarcia/scalaanalytics.git', :tag => "#{s.version}" }
    
    s.platform     = :ios, "9.0"
    s.swift_versions = ['5.0', '5.1', '5.2', '5.3', '5.4', '5.5', '5.6']
    
    s.static_framework = true
    
    s.ios.vendored_frameworks = 'Sources/Frameworks/ScalaAnalytics.xcframework'
    
    end
    