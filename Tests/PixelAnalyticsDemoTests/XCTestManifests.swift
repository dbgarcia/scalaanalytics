import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(PixelAnalyticsDemoTests.allTests),
    ]
}
#endif
