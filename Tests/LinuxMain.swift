import XCTest

import PixelAnalyticsDemoTests

var tests = [XCTestCaseEntry]()
tests += PixelAnalyticsDemoTests.allTests()
XCTMain(tests)
