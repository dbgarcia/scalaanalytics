# ScalaAnalytics for iOS

## Example ScalaAnalytics
In this repo there is a demo, it is in the "Example/" folder

* DemoSwiftPackageManager (is Swift Package Manager)
* DemoCocoapods (with file Podfile)


## Installing ScalaAnalytics
ScalaAnalytics supports [CocoaPods](https://cocoapods.org/) (static). ScalaAnalytics is written in ***Swift 5***.

### CocoaPods
Add the source to your Podfile:
```ruby
source 'https://gitlab.com/dbgarcia/scalaanalytics.git'
```

Add the pod to your Podfile:
```ruby
pod 'ScalaAnalytics'
```

And then run:
```ruby
pod install --repo-update
```
Or update version:
```ruby
pod update ScalaAnalytics
```
After installing the cocoapod into your project import ScalaAnalytics with 
```swift
import ScalaAnalytics
```

### Swift Package Manager

[Swift Package Manager(SPM)](https://swift.org/package-manager/) is Apple's dependency manager tool. It is now supported in Xcode 11. So it can be used in all appleOS types of projects. It can be used alongside other tools like CocoaPods and Carthage as well. 

To install ScalaAnalytics package into your packages, add a reference to ScalaAnalytics and a targeting release version in the dependencies section in `Package.swift` file:

```swift
import PackageDescription

let package = Package(
    name: "YOUR_PROJECT_NAME",
    products: [],
    dependencies: [
        .package(url: "https://gitlab.com/dbgarcia/scalaanalytics.git", from: "1.0.0")
    ]
)
```

To install ScalaAnalytics package via Xcode

 * Go to File -> Swift Packages -> Add Package Dependency...
 * Then search for https://gitlab.com/dbgarcia/scalaanalytics.git
 * And choose the version you want



LICENSE
---
Distributed under the MIT License.
