//
//  ScalaAnalytics.h
//  ScalaAnalytics
//
//  Created by Douglas Garcia Barroso on 24/09/20.
//

#import <Foundation/Foundation.h>

//! Project version number for ScalaAnalytics.
FOUNDATION_EXPORT double ScalaAnalyticsVersionNumber;

//! Project version string for ScalaAnalytics.
FOUNDATION_EXPORT const unsigned char ScalaAnalyticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ScalaAnalytics/PublicHeader.h>


